#!/bin/bash -e
#####################################################################################
# (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################

. $(dirname $0)/env_setup.sh

# make sure we do not re-run cmake
find ${BUILDDIR} -type f -exec touch -d $(date +@%s) \{} \;

mkdir -p ${BUILDDIR}/html
( make BUILDDIR=${BUILDDIR} test ARGS='-j4' || touch ${BUILDDIR}/html/tests_failed ) | tee ${BUILDDIR}/ctest.log
mv ${BUILDDIR}/html ${TESTS_REPORT}
mv ${BUILDDIR}/ctest.log ${TESTS_REPORT}
mv ${BUILDDIR}/Testing ${TESTS_REPORT}
if [ -e ${TESTS_REPORT}/tests_failed ] ; then
  # this prints all lines starting with a white space after and including "The following tests...", excluding lines with "Not Run"
  echo "================================================================================"
  awk '/^[^[:space:]]/{do_print=0}; /The following tests FAILED:/{do_print=1}; do_print&&!/Not Run/{print}' ${TESTS_REPORT}/ctest.log
  echo "================================================================================"
  exit 1
fi
